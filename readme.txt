=== DS Core ===
Contributors: masterida
Donate link: https://www.gff.ch/
Requires at least: 5.8
Tested up to: 6.1
Requires PHP: 7.4
Stable tag: 1.2
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

The DS Core plugin.

== Description ==

The **DS Core** plugin.

== Changelog ==

= 1.2 =
* Add `phpoffice/phpword` library.

= 1.1 =
* Add PUCv5.

= 1.0 =
* Initial version.
