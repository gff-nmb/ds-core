<?php

declare( strict_types=1 );

/**
 * Plugin Name:       DS Core
 * Description:       This plugin contains libraries that would be required by many DS plugins.
 * Requires at least: 5.8
 * Requires PHP:      7.4
 * Version:           1.2
 * Author:            A. Suter
 * Author URI:        https://www.gff.ch/
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       ds-core
 */

use YahnisElsts\PluginUpdateChecker\v5\PucFactory;
use YahnisElsts\PluginUpdateChecker\v5p0\Vcs\GitLabApi;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * Absolute filesystem path to the `ds-core` plugin root file.
 */
define( 'DS_CORE_ABS_PLUGIN_FILE', __FILE__ );

/**
 * Absolute filesystem path to the `ds-core` plugin.
 */
define( 'DS_CORE_ABS_PATH', dirname( __FILE__ ) );

/**
 * The DS Core Build Number (would only be increased if a library had been updated).
 */
define( 'DS_CORE_BUILD_NUMBER', 2 );


///
// COMPOSER AUTOLOAD
///
include( DS_CORE_ABS_PATH . '/vendor/autoload.php' );

///////////////////////////// PLUGIN UPDATE CHECKER ////////////////////////////

call_user_func( function () {
	$group_name  = 'gff-nmb';
	$plugin_name = basename( __FILE__, '.php' );

	if ( class_exists( PucFactory::class ) ) {
		$update_checker = PucFactory::buildUpdateChecker(
			'https://gitlab.com/' . $group_name . '/' . $plugin_name . '/',
			__FILE__
		);

		/** @var GitLabApi $vcs_api */
		$vcs_api = $update_checker->getVcsApi();
		$vcs_api->enableReleasePackages();
	} elseif ( class_exists( Puc_v4_Factory::class ) ) {
		$update_checker = Puc_v4_Factory::buildUpdateChecker(
			'https://gitlab.com/' . $group_name . '/' . $plugin_name . '/',
			__FILE__
		);

		/** @var Puc_v4p11_Vcs_GitLabApi $vcs_api */
		$vcs_api = $update_checker->getVcsApi();
		$vcs_api->enableReleasePackages();
	}
} );
