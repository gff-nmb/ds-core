# DS Core

The WordPress Plugin **DS Core**.

## Dependencies

* None

## Authors and acknowledgment

Adrian Suter, [adrian.suter@gff.ch](mailto:adrian.suter@gff.ch)

## Copyright

[GFF Integrative Kommunikation GmbH](https://www.gff.ch/)

## License

[GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

## Development

### Staging

Always use a staging environment to develop and test things out before releasing.

### Build a new release

1. Set the new version `X.Y` in the file comments section of `ds-core.php`.
   Commit and push.
2. Add a new repository tag `X.Y` without writing release notes (a message is
   allowed).
4. Gitlab CI would automatically create a release asset zip file.
5. Once finished (see pipeline or release section), update the stable version in
   `readme.txt` to `X.Y` and commit and push.
